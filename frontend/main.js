
const Api={
    baseUrl:'http://localhost:8080',
}
const start=()=>{
  fetch(Api.baseUrl+`/Employees`).then(reponse=>reponse.json()).then(data=>{
    renderTable(data)
 })
}
const renderTable =(data)=>{
      var html ='';
      data.forEach((e,index) => {
          html+=`
          
          <tr>
          <th scope="row">${index+1}</th>
         
          <td>${e.fullName}</td>
          <td>${e.address}</td>
          <td> 
          <span onclick="delhanle(${e.id})">
          <i class="fa-solid fa-trash-can"></i>
          </span> 
          <span>
          <i class="fa-solid fa-calendar-day"></i>
          </span>
          </td>
         
        </tr>`;
      });
      document.querySelector("#data-table").innerHTML =html;
}
const delhanle=(id)=>{
      fetch(Api.baseUrl+`/Employess/${id}`,{
        method: 'DELETE'
      }).then(res=>res.json).then(data=>{
          console.log(data)
      })
      start();
}
const Search=()=>{
    var keyword = document.querySelector("#keyword").value
    fetch(Api.baseUrl+`/Employees?id=${keyword}`).then(reponse=>reponse.json()).then(data=>{
        renderTable(data)
     })
}
start();