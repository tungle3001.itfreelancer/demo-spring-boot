package com.shopbichdu.shopme.ApiController;

import com.shopbichdu.shopme.Domain.Employee;

import com.shopbichdu.shopme.Domain.ResponseDataMessage;
import com.shopbichdu.shopme.Repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/Employees")

public class EmployeeController {

    @Autowired
    public EmployeeRepository employeeRepository;

    @GetMapping(value = {"/",""})//Get all employees or find by id
    public ResponseEntity<List<Employee>>getAll(@RequestParam(value = "id",required = false) String id){

        List<Employee> employees =new ArrayList<>();
        if(id==null){
            employees   = employeeRepository.getAllEmployees();
        }else {
            employees = employeeRepository.findEmployeeById(id);
        }
        return  ResponseEntity.ok(employees);
    }
    @PostMapping(value = {"/",""})//Add employee
    public ResponseEntity<?>add(@RequestBody Employee employee){
        System.out.print(employee.toString());
      Object obj=  employeeRepository.addEmployee(employee);
      return ResponseEntity.ok(new ResponseDataMessage(
              obj,
              "Add Success full",
              200
      ));
    }
    @CrossOrigin(origins = "http://localhost:8080")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> del(@PathVariable String id){
        System.out.println(id);
        Object obj = employeeRepository.DelById(id);
        return ResponseEntity.ok(obj);
    }


}
