package com.shopbichdu.shopme.Domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ResponseDataMessage {


        public Object data;

        public String message;

        public int code;

}
