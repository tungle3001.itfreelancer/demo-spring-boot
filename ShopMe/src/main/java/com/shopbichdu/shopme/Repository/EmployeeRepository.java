package com.shopbichdu.shopme.Repository;

import com.shopbichdu.shopme.Domain.Employee;
import com.shopbichdu.shopme.Domain.ResponseObject;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@Repository
public class EmployeeRepository {

   private List<Employee> employees = new ArrayList<>();

  public EmployeeRepository(){
      employees.add(new Employee("1","ThanhTung","Thua Thien Hue"));
      employees.add(new Employee("2","BichDu","Thua Thien Hue"));
      employees.add(new Employee("3","ThanhDat","UKU"));
      employees.add(new Employee("4","Viet","Russia"));
      employees.add(new Employee("5","Dung","Thua Thien Hue"));
   }
    public List<Employee> getAllEmployees(){
        return employees;
    }

    public List<Employee> addEmployee(Employee employee){

         employees.add(employee);
         return  employees;
    }
    public Object DelById(String id){
      int lengthOrigin = employees.size();

        Iterator<Employee>itrs = employees.listIterator();
        while (itrs.hasNext()){
            Employee e = itrs.next();
            if(e.getId().equals(id)){
                itrs.remove();
            }
      };
      if(employees.size()!=lengthOrigin){
       return
               new ResponseObject("Del Success full !",200);
      }else{
        return
                new ResponseObject("Not exit with employee have id="+id+" !",200);
      }

    }
    public List<Employee> findEmployeeById(String name){
        List<Employee> contains = new ArrayList<>();
        employees.forEach(employee -> {
            if(employee.getFullName().contains(name)){
                contains.add(employee);
            }
        });
        return
                contains;
    }


}
